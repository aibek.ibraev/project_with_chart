import React, {useState} from 'react'
import {
  BrowserRouter as Router,
  Switch
} from "react-router-dom";
import HomePage from "./pages/HomePage";
import ScrollToTop from "./utility/scrollToTop";
import SignIn from "./pages/SignIn";
import PublicRoute from "./routes/PublicRoutes";
import PrivateRoute from "./routes/PrivateRoutes";
import FAQPage from "./pages/FAQPage";
import MyProfilePage from "./pages/MyProfilePage";
import UsersPage from "./pages/UsersPage";
import AddUsersPage from "./pages/AddUsersPage";
import UsersDetailPage from "./pages/UsersDetailPage";
import AddDepartamentPage from "./pages/AddDepartamentPage";
import DepartamentDetailPage from "./pages/DepartamentDetailPage";

function App() {
  const [isLogged, setIsLogged] = useState(localStorage.getItem('user'))
  return (
      <Router>
        <Switch>
          <PublicRoute component={SignIn} isLogged={isLogged} path="/signIn" exact />
          <PrivateRoute component={HomePage} isLogged={isLogged} path="/" exact />
          <PrivateRoute component={MyProfilePage} isLogged={isLogged} path="/myprofile" exact />
          <PrivateRoute component={DepartamentDetailPage} isLogged={isLogged} path="/case/1" exact />
          <PrivateRoute component={AddDepartamentPage} isLogged={isLogged} path="/case/add" exact />
          <PrivateRoute component={AddUsersPage} isLogged={isLogged} path="/users/add" exact />
          <PrivateRoute component={FAQPage} isLogged={isLogged} path="/faq" exact />
          <PrivateRoute component={UsersPage} isLogged={isLogged} path="/users" exact />
          <PrivateRoute component={UsersDetailPage} isLogged={isLogged} path="/users/1" exact />
        </Switch>

        <ScrollToTop/>
      </Router>
  );
}

export default App;

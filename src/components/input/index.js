import React from 'react';

const Input = ({padding=10,type, placeholder,key, handler}) => {
    const styles = {
        width: '100%',
        backgroundColor: '#ffffff',
        padding: padding,
        border: '1px solid #ccc',
        borderRadius: '10px',
        cursor: 'pointer',
        marginBottom: '20px',
        outline:'none',
    }
    return (
        <input
            style={styles}
            onChange={(e) => handler(key, e.target.value)}
            type={type}
            placeholder={placeholder}
        />
    );
};

export default Input;
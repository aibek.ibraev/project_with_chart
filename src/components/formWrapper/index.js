import React from 'react';
import Input from "../input";
import styles from './style.module.scss';
import Button from "../button";

const FormWrapper = () => {

    const [submitObj, setSubmitObj] = React.useState({
        first_name: '',
        last_name: '',
        phone_number: '',
        case_number: ''
    })

    const handleSetObj = (key, value) => {
        setSubmitObj(old => ({...old, [key]: value}))
    }
    const handleSubmitForm = () => {
        alert(`Отправлено ${JSON.parse(submitObj)}`)
    }

    return (
        <>
            <div className="container">
                <form className={styles.form_wrapper}>
                    <Input type={'text'} placeholder={'Введите имя'} key={'first_name'} handler={handleSetObj}/>
                    <Input type={'text'} placeholder={'Введите фамилие'} key={'last_name'} handler={handleSetObj}/>
                    <Input type={'text'} placeholder={'Введите номер дела'} key={'case_number'} handler={handleSetObj}/>
                    <Input type={'text'} placeholder={'Введите номер телефона'} key={'phone_number'} handler={handleSetObj}/>
                    <Input type={'text'} placeholder={'Введите ююю'}/>
                    <Input type={'text'} placeholder={'Введите ююю'}/>
                    <Input type={'text'} placeholder={'Введите ююю'}/>
                    <p>отметка Следователя Криминалистом </p>
                    <Button
                        value={'Отправить'}
                        color={'black'}
                        background={'#ADD800'}
                        handler={handleSubmitForm}
                        padding={'20px 50px'}
                    />
                </form>
            </div>
        </>
    );
};

export default FormWrapper;
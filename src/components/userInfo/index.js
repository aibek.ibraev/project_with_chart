import React from 'react';
import styles from './style.module.scss';

const UserInfo = () => {
    return (
        <div className={styles.profile_info_wrapper}>
            <div className="container">
                <div className={styles.profile_info_inner}>
                    <div className={styles.profile_info_image}>
                        <img
                            src="https://cdn1.vectorstock.com/i/1000x1000/23/70/man-avatar-icon-flat-vector-19152370.jpg"
                            alt="avatar"
                        />
                    </div>
                    <div className={styles.profile_info_text}>
                        <div className={styles.profile_info_item}>
                            <span>name:</span>
                            <p>Ibraev Aibek</p>
                        </div>
                        <div className={styles.profile_info_item}>
                            <span>position:</span>
                            <p>Front-end developer</p>
                        </div>
                        <div className={styles.profile_info_item}>
                            <span>specialization:</span>
                            <p>Development</p>
                        </div>
                        <div className={styles.profile_info_item}>
                            <span>departament:</span>
                            <p>Front-end</p>
                        </div>
                        <div className={styles.profile_info_item}>
                            <span>education:</span>
                            <p>KSLA Kyrgyzstan, Bishkek</p>
                        </div>
                        <div className={styles.profile_info_item}>
                            <span>name:</span>
                            <p>Ibraev Aibek</p>
                        </div>
                        <div className={styles.profile_info_item}>
                            <span>name:</span>
                            <p>Ibraev Aibek</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default UserInfo;
import React from 'react';
import styles from './style.module.scss';
import SearchIcon from '../../assets/images/icon-search.svg'

const SearchBar = ({searchTitle}) => {
    return (
        <div class={styles.input_wrapper}>
            <img src={SearchIcon} alt="search"/>
            <input
                type="text"
                placeholder={searchTitle}
            />
        </div>
    );
};

export default SearchBar;
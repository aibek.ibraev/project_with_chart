import React from 'react';
import styles from './style.module.scss';
import classNames from "classnames";

const FilterBar = () => {
    return (
        <div className={styles.filter_items}>
            <div className={classNames(styles.filter_item, styles.active) }>
                <p>All</p>
            </div>
            <div className={styles.filter_item}>
                <p>Front-end</p>
            </div>
            <div className={styles.filter_item}>
                <p>Back-end</p>
            </div>
            <div className={styles.filter_item}>
                <p>IOS</p>
            </div>
            <div className={styles.filter_item}>
                <p>Android</p>
            </div>
            <div className={styles.filter_item}>
                <p>Sales</p>
            </div>
            <div className={styles.filter_item}>
                <p>Product</p>
            </div>
        </div>
    );
};

export default FilterBar;
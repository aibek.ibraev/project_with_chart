import React from 'react';
import styles from './style.module.scss';
import CaseItem from "../caseItem";

const CaseWrapper = () => {
    return (
        <div className={styles.case_wrapper}>
            <div className={styles.case_wrapper_title}>
                <p>All departments:</p>
                <span>Show 105 из 105 departments</span>
            </div>
            <div className={styles.case_wrapper_items}>
                <CaseItem/>
                <CaseItem/>
                <CaseItem/>
                <CaseItem/>
                <CaseItem/>
                <CaseItem/>
            </div>
        </div>
    );
};

export default CaseWrapper;
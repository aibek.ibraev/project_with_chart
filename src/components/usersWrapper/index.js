import React from 'react';
import styles from './style.module.scss';
import UsersItem from "../usersItem";

const UsersWrapper = () => {
    return (
        <div className={styles.case_wrapper}>
            <div className={styles.case_wrapper_title}>
                <p>All staff</p>
                <span>Show 105 из 105 staff</span>
            </div>
            <div className={styles.case_wrapper_items}>
                <UsersItem/>
                <UsersItem/>
                <UsersItem/>
                <UsersItem/>
                <UsersItem/>
                <UsersItem/>

            </div>
        </div>
    );
};

export default UsersWrapper;
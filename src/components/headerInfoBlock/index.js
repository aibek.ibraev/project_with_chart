import React from 'react';
import styles from './style.module.scss';
import Button from "../button";
import ArticleIcon from "@mui/icons-material/Article";

const HeaderInfoBlock = ({title, button, buttonTitle, icon}) => {
    return (
        <div className={styles.header_info_block}>
            <div className="container">
                <div className={styles.header_info_block_inner}>
                    <div  className={styles.header_info_block_left}>
                        {icon}

                        <h1>{title}</h1>
                    </div>
                    <div  className={styles.header_info_block_right}>
                        {
                            button
                            &&
                            <Button
                                uppercase={'uppercase'}
                                padding={'11px 14px'}
                                color={'#ffffff'}
                                background={'#40D2BF'}
                                value={buttonTitle}
                                handler={() => console.log('create')}
                            />
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeaderInfoBlock;
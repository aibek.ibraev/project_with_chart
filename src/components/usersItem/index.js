import React from 'react';
import styles from './style.module.scss';
import {useHistory} from "react-router-dom";
import User from '../../assets/images/user-photo.png'

const UsersItem = () => {
    const history = useHistory()

    const onDetailPage = () => {
        history.push('/users/1')
    }

    return (
        <div className={styles.case_item} onClick={onDetailPage}>
            <div className={styles.case_item_top}>
                <div className={styles.case_item_top_inner}>
                    <div>
                        <img src={User} alt="#"/>
                    </div>
                    <div>
                        <p>Ibraev Aibek</p>
                        <span>created at 15.01.2018</span>
                    </div>
                </div>
                <div className={styles.case_item_top_description}>
                    <p>position - Front-End developer</p>
                    <p>the Department - Front-End </p>
                    <p>specialization - Applied Informatics </p>
                </div>
            </div>
        </div>
    );
};

export default UsersItem;
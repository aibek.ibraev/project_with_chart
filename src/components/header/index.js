import React from 'react';
import styles from './style.module.scss'
import userPhoto from '../../assets/images/user-photo.png'
import arrow from '../../assets/images/arrow.svg'
import {Link, useHistory} from "react-router-dom";
import Button from "../button";

const Header = () => {
    const [openDropdown, setOpenDropDown] = React.useState(false)
    const history = useHistory()

    const logOutHandler = () => {
        console.log('Выйти')
        localStorage.setItem('user', '');
    }
    const toMyProfileHanlder = () => {
        history.push('/profile')
    }
    const toAddCaseHanlder = () => {
        history.push('/case/add')
    }

    return (
        <div className={styles.header}>
            <div className="container">
                <div className={styles.header_inner}>
                    <div className={styles.left_blocks}>
                        <div className={styles.left_blocks_one}>
                            <p>NARWHAL</p>
                        </div>
                        <div className={styles.left_blocks_two}>
                            <p>Teams</p>
                        </div>
                    </div>

                    <div className={styles.right_blocks} onClick={() => setOpenDropDown(!openDropdown)}>
                        <p>Hello, Aibek !</p>
                        <div className={styles.user_block_photo}>
                            <img src={userPhoto} alt="user-photo"/>
                        </div>
                        <div
                            className={styles.user_block_arrow}
                            style={ openDropdown ? { 'transform': 'rotate(180deg)'} : {}}
                        >
                            <img src={arrow} alt="arrow"/>
                        </div>
                        {
                            openDropdown
                            &&
                            <div className={styles.user_dropdown_item_wrapper}>
                                <div className={styles.user_dropdown_item}>
                                    <button onClick={toMyProfileHanlder}>Мой профиль</button>
                                    <button onClick={logOutHandler}>Выйти</button>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>

        </div>
    );
};

export default Header;
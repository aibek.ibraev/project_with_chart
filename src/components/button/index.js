import React from 'react';

const Button = ({background, value, color,padding=20, handler, uppercase='capitalize'}) => {
    const styles = {
        backgroundColor: background,
        color: color,
        padding: padding,
        border: 'none',
        borderRadius: '4px',
        cursor: 'pointer',
        textTransform: uppercase,
    }

    const buttonClickHandler = (e) => {
        e.stopPropagation()
        e.preventDefault()
        handler()
    }
    return (
        <button
            style={styles}
            onClick={(e) => buttonClickHandler(e)}
        >
            {value}
        </button>
    );
};

export default Button;
import React, {useEffect, useState} from 'react';
import styles from './style.module.scss';
import {
    Chart as ChartJS,
    LinearScale,
    CategoryScale,
    BarElement,
    PointElement,
    LineElement,
    Legend,
    Tooltip,
    ArcElement,
} from 'chart.js';
import { Chart,Pie } from 'react-chartjs-2';
import {data2, data3, dataMonths, dataWeek, dataYears} from "../../data";

ChartJS.register(
    ArcElement,
    LinearScale,
    CategoryScale,
    BarElement,
    PointElement,
    LineElement,
    Legend,
    Tooltip
);



const DepartamentDetail = () => {
    const [dataReport, setDataReport] = useState(dataWeek)

    useEffect(() => {
        setDataReport(dataWeek)
    },[])

    const onSelectReport = (e) => {
        e.preventDefault();
        e.stopPropagation();
        switch (e.target.value) {
            case 'week':
                setDataReport(dataWeek)
                break;
            case 'months':
                setDataReport(dataMonths)
                break;
            case 'years':
                setDataReport(dataYears)
                break;
            default:
                setDataReport(dataWeek)
                break;
        }
    }

    return (
        <div className={styles.case_wrapper}>
            <h2>Report for:</h2>
            <select
                name="report"
                id="report"
                form="reportform"
                className={styles.report}
                onChange={e => onSelectReport(e)}
            >
                    <option value="week">week</option>
                    <option value="months">month</option>
                    <option value="years">year</option>
            </select>
            <Chart type='bar' data={dataReport} />
            <h2>Closed tasks:</h2>
            <Pie data={data2} />
            <h2>Comparison with other departments:</h2>
            <Chart type='bar' data={data3} />
        </div>
    );
};

export default DepartamentDetail;
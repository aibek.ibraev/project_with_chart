import React from 'react';
import ArticleIcon from '@mui/icons-material/Article';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import AddCardIcon from '@mui/icons-material/AddCard';
import QuizIcon from '@mui/icons-material/Quiz';
import {IconButton} from "@mui/material";
import styles from './style.module.scss';
import classNames from "classnames";
import {Link} from "react-router-dom";

const Sidebar = () => {
    return (
            <ul className={styles.icons_list}>
                <li className={window.location.pathname === '/myprofile' ? classNames(styles.icons_list_item, styles.active) : styles.icons_list_item}>
                    <Link to='/myprofile'>
                        <IconButton>
                            <AccountCircleIcon  style={{ fontSize: 33}}/>
                        </IconButton>
                    </Link>
                </li>
                <li className={window.location.pathname === '/' && '/case/1' ? classNames(styles.icons_list_item, styles.active) : styles.icons_list_item}>
                    <Link to='/'>
                        <IconButton>
                            <ArticleIcon  style={{ fontSize: 33}}/>
                        </IconButton>
                    </Link>
                </li>
                <li className={window.location.pathname === '/users' && '/users/1' ? classNames(styles.icons_list_item, styles.active) : styles.icons_list_item}>
                    <Link to='/users'>
                        <IconButton>
                            <SupervisorAccountIcon  style={{ fontSize: 33}}/>
                        </IconButton>
                    </Link>
                </li>
                <li className={window.location.pathname === '/case/add' ? classNames(styles.icons_list_item, styles.active) : styles.icons_list_item}>
                    <Link to='/case/add'>
                        <IconButton>
                            <AddCardIcon  style={{ fontSize: 33}}/>
                        </IconButton>
                    </Link>
                </li>
                <li className={window.location.pathname === '/users/add' ? classNames(styles.icons_list_item, styles.active) : styles.icons_list_item}>
                    <Link to='/users/add'>
                        <IconButton>
                            <GroupAddIcon  style={{ fontSize: 33}}/>
                        </IconButton>
                    </Link>
                </li>
                <li className={window.location.pathname === '/faq' ? classNames(styles.icons_list_item, styles.faq, styles.active) : classNames(styles.faq, styles.icons_list_item)}>
                    <Link to='/faq'>
                        <IconButton>
                            <QuizIcon  style={{ fontSize: 33}}/>
                        </IconButton>
                    </Link>
                </li>
            </ul>
    );
};

export default Sidebar;
import React from 'react';
import FilterBar from "../filterbar";
import SearchBar from "../searchbar";
import styles from './style.module.scss';

const CaseFilter = ({searchTitle}) => {
    return (
        <div className={styles.case_filter_wrapper}>
            <div className='d-flex space-between container'>
                <FilterBar/>
                <SearchBar searchTitle={searchTitle}/>
            </div>
        </div>
    );
};

export default CaseFilter;
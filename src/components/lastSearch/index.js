import React from 'react';
import styles from './style.module.scss';
import User from '../../assets/images/user-photo.png';


const LastSearch = () => {
    return (
        <div className={styles.last_search_wrapper}>
            <p className={styles.title}>Latest search results:</p>
            <div className={styles.last_search_items}>
                <div className={styles.last_search_item}>
                    <div className={styles.last_search_head}>
                        <div className={styles.last_search_user_photo}>
                            <img src={User} alt="user"/>
                        </div>
                        <div className={styles.last_search_case_info}>
                            <p>Ibraev Aibek</p>
                            <span>Created at 15.09.2021</span>
                        </div>
                    </div>
                    <div className={styles.last_search_desc}>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, error!</p>
                    </div>
                </div>
                <div className={styles.last_search_item}>
                    <div className={styles.last_search_head}>
                        <div className={styles.last_search_user_photo}>
                            <img src={User} alt="user"/>
                        </div>
                        <div className={styles.last_search_case_info}>
                            <p>Ibraev Aibek</p>
                            <span>Created at 15.09.2021</span>
                        </div>
                    </div>
                    <div className={styles.last_search_desc}>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, error!</p>
                    </div>
                </div>
                <div className={styles.last_search_item}>
                    <div className={styles.last_search_head}>
                        <div className={styles.last_search_user_photo}>
                            <img src={User} alt="user"/>
                        </div>
                        <div className={styles.last_search_case_info}>
                            <p>Ibraev Aibek</p>
                            <span>Created at 15.09.2021</span>
                        </div>
                    </div>
                    <div className={styles.last_search_desc}>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, error!</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LastSearch;
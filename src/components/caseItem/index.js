import React from 'react';
import styles from './style.module.scss';
import {useHistory} from "react-router-dom";
import User from '../../assets/images/user-photo.png'

const CaseItem = () => {
    const history = useHistory()

    const onDetailPage = () => {
        history.push('/case/1')
    }

    return (
        <div className={styles.case_item} onClick={onDetailPage}>
            <div className={styles.case_item_top}>
                <div className={styles.case_item_top_inner}>
                    <div>
                        <img src={User} alt="#"/>
                    </div>
                    <div>
                        <p>front-end department</p>
                        <span>Created at 15.01.2018</span>
                    </div>
                </div>
                <div className={styles.case_item_top_description}>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda commodi dolorem doloremque, expedita facilis maxime molestias possimus quibusdam veniam voluptate!
                    </p>
                </div>
            </div>
        </div>
    );
};

export default CaseItem;
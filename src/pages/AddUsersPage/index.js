import React from 'react';
import Header from "../../components/header";
import Sidebar from "../../components/sidebar";
import HeaderInfoBlock from "../../components/headerInfoBlock";
import CaseFilter from "../../components/caseFilter";
import LastSearch from "../../components/lastSearch";
import FormWrapper from "../../components/formWrapper";
import GroupAddIcon from "@mui/icons-material/GroupAdd";

const AddUsersPage = () => {
    return (
        <div class='d-flex space-between'>
            <div class='w-5 dark-bg '>
                <Sidebar/>
            </div>
            <div className='w-95'>
                <Header/>
                <HeaderInfoBlock
                    title={'Add Staff'}
                    button={false}
                    icon={<GroupAddIcon  style={{ fontSize: 33}}/>}
                />
                <div class='d-flex space-between container mt-50'>
                    <FormWrapper/>
                </div>
            </div>
        </div>
    );
};

export default AddUsersPage;
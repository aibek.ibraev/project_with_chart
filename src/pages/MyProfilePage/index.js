import React from 'react';
import Header from "../../components/header";
import CaseWrapper from "../../components/caseWrapper";
import Sidebar from "../../components/sidebar";
import HeaderInfoBlock from "../../components/headerInfoBlock";
import LastSearch from "../../components/lastSearch";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import UserInfo from "../../components/userInfo";
import ProfileWrapper from "../../components/profileWrapper";

const MyProfilePage = () => {
    return (
        <div class='d-flex space-between'>
            <div class='w-5 dark-bg '>
                <Sidebar/>
            </div>
            <div className='w-95'>
                <Header/>
                <HeaderInfoBlock
                    title={'My Profile'}
                    button={true}
                    buttonTitle={'Изменить'}
                    icon={ <AccountCircleIcon  style={{ fontSize: 33}}/>}
                />
                <UserInfo/>
                <div class='d-flex space-between container mt-50'>
                    <ProfileWrapper/>
                    <LastSearch/>
                </div>
            </div>
        </div>
    );
};

export default MyProfilePage;
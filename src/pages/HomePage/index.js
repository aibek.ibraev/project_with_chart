import React from 'react';
import Header from "../../components/header";
import CaseWrapper from "../../components/caseWrapper";
import Sidebar from "../../components/sidebar";
import HeaderInfoBlock from "../../components/headerInfoBlock";
import CaseFilter from "../../components/caseFilter";
import LastSearch from "../../components/lastSearch";
import ArticleIcon from "@mui/icons-material/Article";

const HomePage = () => {
    return (
        <div class='d-flex space-between'>
            <div class='w-5 dark-bg '>
                <Sidebar/>
            </div>
            <div className='w-95'>
                <Header/>
                <HeaderInfoBlock
                    title={'Departaments'}
                    button={true}
                    buttonTitle={'+ Create new departaments'}
                    icon={<ArticleIcon  style={{ fontSize: 33}}/>}
                />
                <CaseFilter searchTitle={'Search departaments ...'}/>
                <div class='d-flex space-between container mt-50'>
                    <CaseWrapper/>
                    <LastSearch/>
                </div>
            </div>
        </div>
    );
};

export default HomePage;
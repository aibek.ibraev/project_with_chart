import React from 'react';
import Header from "../../components/header";
import Sidebar from "../../components/sidebar";
import HeaderInfoBlock from "../../components/headerInfoBlock";
import CaseFilter from "../../components/caseFilter";
import LastSearch from "../../components/lastSearch";
import UsersWrapper from "../../components/usersWrapper";
import SupervisorAccountIcon from "@mui/icons-material/SupervisorAccount";

const UsersPage = () => {
    return (
        <div class='d-flex space-between'>
            <div class='w-5 dark-bg '>
                <Sidebar/>
            </div>
            <div className='w-95'>
                <Header/>
                <HeaderInfoBlock
                    title={'Staff'}
                    button={true}
                    buttonTitle={'+ Create new staff'}
                    icon={<SupervisorAccountIcon  style={{ fontSize: 33}}/>}
                />
                <CaseFilter searchTitle={'Search all staff ...'}/>
                <div class='d-flex space-between container mt-50'>
                    <UsersWrapper/>
                    <LastSearch/>
                </div>
            </div>
        </div>
    );
};

export default UsersPage;
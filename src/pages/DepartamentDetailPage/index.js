import React from 'react';
import Header from "../../components/header";
import Sidebar from "../../components/sidebar";
import CaseWrapper from "../../components/caseWrapper";
import LastSearch from "../../components/lastSearch";
import DepartamentDetail from "../../components/departamentDetail";
import HeaderInfoBlock from "../../components/headerInfoBlock";
import ArticleIcon from "@mui/icons-material/Article";

const DepartamentDetailPage = () => {
    return (
        <div className='d-flex space-between'>
            <div className='w-5 dark-bg '>
                <Sidebar/>
            </div>
            <div className='w-95'>
                <Header/>
                <HeaderInfoBlock
                    title={'Front-end Departament'}
                    button={false}
                    icon={<ArticleIcon  style={{ fontSize: 33}}/>}
                />
                <div className='d-flex space-between container mt-50'>
                    <DepartamentDetail/>
                    <LastSearch/>
                </div>
            </div>
        </div>
    );
};

export default DepartamentDetailPage;



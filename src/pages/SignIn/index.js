import React from 'react';
import styles from './style.module.scss'
import eyeIcon from '../../assets/images/visible.svg'

const SignIn = () => {
    const [visible, setVisible] = React.useState(false);
    const [value, setValue] = React.useState({
        username: '',
        password: '',
        email:  '',
    })

    const setInputsValue = (key, value) => {
        setValue(old => ({...old, [key]: value}))
    }
    const signInHandler = () => {
        console.log('Вошел')
        localStorage.setItem('user', 'true');
    }

    return (
        <div className={styles.login_page}>
            <div className={styles.login_page_wrapper}>
                <div className={styles.form_block}>
                    <div className={styles.form_block_inner}>
                        <h1>Войти</h1>
                        <input
                            type="text"
                            placeholder={'Логин'}
                            value={value.login}
                            onChange={e => setInputsValue('username', e.target.value)}
                        />
                        <div className={styles.password_input}>
                            <input
                                type={!visible ? "password" : "text"}
                                placeholder={'Пароль'}
                                value={value.password}
                                onChange={e => setInputsValue('password', e.target.value)}
                            />
                            <img
                                src={eyeIcon}
                                alt='123'
                                onClick={() => setVisible(!visible)}
                            />
                        </div>
                        <button onClick={signInHandler}>Войти</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SignIn;
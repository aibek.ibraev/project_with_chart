import React from 'react';
import Header from "../../components/header";
import CaseWrapper from "../../components/caseWrapper";
import Sidebar from "../../components/sidebar";
import HeaderInfoBlock from "../../components/headerInfoBlock";
import LastSearch from "../../components/lastSearch";
import QuizIcon from "@mui/icons-material/Quiz";

const FAQPage = () => {
    return (
        <div class='d-flex space-between'>
            <div class='w-5 dark-bg '>
                <Sidebar/>
            </div>
            <div className='w-95'>
                <Header/>
                <HeaderInfoBlock
                    title={'FAQ'}
                    button={false}
                    icon={<QuizIcon  style={{ fontSize: 33}}/>}
                />
                <div class='d-flex space-between container mt-50'>
                    <CaseWrapper/>
                    <LastSearch/>
                </div>
            </div>
        </div>
    );
};

export default FAQPage;
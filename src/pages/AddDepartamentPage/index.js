import React from 'react';
import Header from "../../components/header";
import CaseWrapper from "../../components/caseWrapper";
import Sidebar from "../../components/sidebar";
import HeaderInfoBlock from "../../components/headerInfoBlock";
import CaseFilter from "../../components/caseFilter";
import LastSearch from "../../components/lastSearch";
import FormWrapper from "../../components/formWrapper";
import AddCardIcon from "@mui/icons-material/AddCard";

const AddDepartamentPage = () => {
    return (
        <div class='d-flex space-between'>
            <div class='w-5 dark-bg '>
                <Sidebar/>
            </div>
            <div className='w-95'>
                <Header/>
                <HeaderInfoBlock
                    title={'Add Departament'}
                    button={false}
                    icon={<AddCardIcon  style={{ fontSize: 33}}/>}
                />
                <div class='d-flex space-between container mt-50'>
                    <FormWrapper/>
                    <LastSearch/>
                </div>
            </div>
        </div>
    );
};

export default AddDepartamentPage;
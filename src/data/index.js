const labelsYears = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];
const labelsMonths = [
    '1 week',
    '2 week',
    '3 week',
    '4 week',
];
const labelsWeeks = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
];

export const dataWeek = {
    labels: labelsWeeks,
    datasets: [
        {
            type: 'bar',
            label: 'Dataset 2',
            backgroundColor: 'rgb(75, 192, 192)',
            data:[22,751,311,813,95,664,186, 990, 560, 210, 32, 500],
            borderColor: 'white',
            borderWidth: 2,
        },
        {
            type: 'bar',
            label: 'Dataset 3',
            backgroundColor: 'rgb(53, 162, 235)',
            data:[22,751,311,813,95,664,186, 990, 560, 210, 32, 500],
        },
    ],
};

export const dataMonths = {
    labels: labelsMonths,
    datasets: [
        {
            type: 'bar',
            label: 'Dataset 2',
            backgroundColor: 'rgb(75, 192, 192)',
            data:[22,751,311,813,95,664,186, 990, 560, 210, 32, 500],
            borderColor: 'white',
            borderWidth: 2,
        },
        {
            type: 'bar',
            label: 'Dataset 3',
            backgroundColor: 'rgb(53, 162, 235)',
            data:[22,751,311,813,95,664,186, 990, 560, 210, 32, 500],
        },
    ],
};

export const dataYears = {
    labels: labelsYears,
    datasets: [
        {
            type: 'bar',
            label: 'Dataset 2',
            backgroundColor: 'rgb(75, 192, 192)',
            data:[22,751,311,813,95,664,186, 990, 560, 210, 32, 500],
            borderColor: 'white',
            borderWidth: 2,
        },
        {
            type: 'bar',
            label: 'Dataset 3',
            backgroundColor: 'rgb(53, 162, 235)',
            data:[22,751,311,813,95,664,186, 990, 560, 210, 32, 500],
        },
    ],
};

export const data2 = {
    labels: labelsYears,
    datasets: [
        {
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3,7,5,8,1,2,10],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(243,223,3,0.2)',
                'rgba(248,6,236,0.2)',
                'rgba(241,25,25,0.2)',
                'rgba(255,130,7,0.2)',
                'rgba(5,246,138,0.2)',
                'rgba(3,206,232,0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(243,223,3,0.2)',
                'rgba(248,6,236,0.2)',
                'rgba(241,25,25,0.2)',
                'rgba(255,130,7,0.2)',
                'rgba(5,246,138,0.2)',
                'rgba(3,206,232,0.2)',
            ],
            borderWidth: 2,
        },
    ],
};

export const data3 = {
    labels: labelsYears,
    datasets: [
        {
            type: 'line',
            label: 'Dataset 1',
            borderColor: 'rgb(53,248,7)',
            borderWidth: 2,
            fill: true,
            data:[0,123,222,345,475,571,671, 611, 700, 500, 800, 788],
        },
        {
            type: 'line',
            label: 'Dataset 2',
            borderColor: 'rgb(255, 99, 132)',
            borderWidth: 2,
            fill: false,
            data:[0,223,272,345,275,571,471, 411, 400, 300, 500, 588],
        },
        {
            type: 'line',
            label: 'Dataset 3',
            borderColor: 'rgb(5,17,243)',
            borderWidth: 2,
            fill: false,
            data:[0,300,222,390,132,322,489, 523, 550, 581, 610, 642],
        }
    ],
};